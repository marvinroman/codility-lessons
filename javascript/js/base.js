let test_input = document.getElementById('test_value')
let solution_div = document.getElementById('solution')

test_input.addEventListener('change', function () {
    solution_div.innerHTML = solution(test_input.value)
})