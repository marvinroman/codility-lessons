/*
* Binary Gap 
* 
* Challenge: write an algorithm that will find the largest binary gap for a given integer
* Detailed Challenge: https://app.codility.com/programmers/lessons/1-iterations/binary_gap/
*/

/**
 * 
 * @param {integer} N 
 */
function solution(N) {

    // Find gaps regex 
    // returns multiple values
    // allows for matches against continuous binary gaps 
    // by making the last 1 a non-zero match using a positive lookahead
    const regex = /10+(?=1)/g;

    // convert integer to binary string
    const binary_string = N.toString(2)

    // pull any binary matches from the binary string 
    // returns array of values otherwise null
    const matches = binary_string.match(regex)

    // return zero if no matches were found
    if (matches == null)
        return 0

    // return largest subtract 1 for the initial 1 that is returned as part of the match
    return getLargest(matches) - 1;
}

/**
 * Return length of largest string in matches
 * @param {array} matches 
 */
function getLargest(matches) {
    let largest = 0
    // loop through matches and set largest to current length of match 
    // if larger than largest current value
    for (match in matches) {
        if (matches[match].length > largest)
            largest = matches[match].length
    }

    return largest
}